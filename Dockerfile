# Building stage with a Alpine origin
FROM alpine:3.21 AS alpine

WORKDIR /app

RUN apk add gcc musl-dev go nodejs python3

# C code
COPY c ./c
RUN cd /app/c \
 && gcc -O2 -o ackerman_o2 ackerman.c \
 && gcc -O3 -o ackerman_o3 ackerman.c

# Goland code
COPY go ./go
RUN cd /app/go \
 && go build /app/go/ackerman.go

# JavaScript code
COPY js ./js

# Python code
COPY python/ ./python
#RUN cd /app/python \
# && pixi install

# Building stage with a Debian origin
FROM debian:12-slim AS debian

WORKDIR /app

RUN apt update \
 && apt install -y gcc golang nodejs curl \
 && curl -fsSL https://pixi.sh/install.sh | PIXI_HOME=/usr bash

# C code
COPY c ./c
RUN cd /app/c \
 && gcc -O2 -o ackerman_o2 ackerman.c \
 && gcc -O3 -o ackerman_o3 ackerman.c

# Goland code
COPY go ./go
RUN cd /app/go \
 && go build /app/go/ackerman.go

# JavaScript code
COPY js ./js

# Python code
COPY python/ ./python
RUN cd /app/python \
 &&  pixi install -e py311 -e py312 -e py313 -e py313jit -e pypy310 \
 && pixi clean cache --yes
