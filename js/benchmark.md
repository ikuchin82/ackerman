![](https://kroki.io/vegalite/svg/eNqlUU1PAjEQ_SukJ03WjRgQw827CQePhpBhO8VKd4ptd4Vs-t-dWYKuHye4tDPzXt-8vHZKQwI171QLrsGo5i-d8nwpcDtLqArlgDbcv8W-XqOLfTe6GZHXQjAk9Gq7ionPq0kxGl_zuFXzaTl7yMX5gg5bRlc17Ieqs3L6JapxbYEuc3lfTi7R-9_ktBznZS5UCkDR-FD3yRrrEgaJ21h0mhX6PYXyhAvDFFm4zHlZqBrClvE1BIaRKq-tOOrUYfi8ZTAddsjlewOUbIJkW_HHpZNxaHhao2Iv--FLQwOSaahK1pOwKu98-Ms8LiFfWwInoeAGSQvPB4uUxKtPydcn2cVp7NAkEf69LMow-I_hLh8HrhbP3Lwi6GNk_Tc80kawu9ltPnptavqZZ_9X3yJPEnDO-RPvr-nB)
# OS alpine
## js node
|Function|Runtime|
|-|-|
|ack_stack(4, 1)|5.78|
|ack_levels_max(4, 1)|7.5|
# OS debian
## js node
|Function|Runtime|
|-|-|
|ack_stack(4, 1)|6.4|
|ack_levels_max(4, 1)|5.1|