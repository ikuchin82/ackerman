function ack_recursive(m , n) {
    if(m === 0){
    	return n + 1;
    }else if(n === 0){
    	return ack_recursive(m-1, 1);
    } else {
    	return ack_recursive(m-1, ack_recursive(m, n-1));
    }
}

function ack_stack(m , n) {
	let last_m = [];
	while (true) {
		if(m === 0){
			n += 1;
        	if(last_m.length === 0){
		        return n
        	} else {
        		m = last_m.pop();
	       	}
		} else if(n === 0){
            m -= 1;
            n = 1
		} else {
	        last_m.push(m - 1);
            n -= 1
		}
    }
}

function ack_levels_max(m , n) {
	let levels = [0,0,0,0];
	let max_m = levels.length;
	while (true) {
		if(m === 0){
			n += 1;
        	while (levels[m] === 0) {
        	    m += 1;
        	    if(m === max_m){
					return n
				}
            }
            levels[m] -= 1
		} else if(n === 0){
            m -= 1;
            n = 1
		} else {
	        levels[m - 1] += 1;
            n -= 1
		}
    }
}

// console.time('ack_recursive(4 , 1)');
// ack_recursive(4 , 1);
// console.timeEnd('ack_recursive(4 , 1)');

console.time('ack_stack(4, 1)');
ack_stack(4 , 1);
console.timeEnd('ack_stack(4, 1)');

console.time('ack_levels(4, 1)');
ack_levels_max(4 , 1);
console.timeEnd('ack_levels_max(4, 1)');