# Ackermann

Collection of different algorithms to calculate [Ackermann function](https://en.wikipedia.org/wiki/Ackermann_function).

Ackermann function is a rapidly growing total computable function that is not primitive recursive. Because of it's recursive nature algorithms that use recursion can go very deep and some programming languages doesn't support it. That's way this code base also content algorithms that are not recursive.

Benchmarking different algorithms and it's implementations in different programming languages. Also testing how the same algorithms performing on [Glibc](https://ru.wikipedia.org/wiki/Glibc) and [Musl](https://ru.wikipedia.org/wiki/Musl).

Currently supporting following programming languages:
- C
- Go
- JavaScript
- Python
  > Python implementation show how different technics can significantly speed up code execution.
  - Pure python implementation
  - Python implementation with [numba](https://numba.pydata.org) JIT
  - [Cythonized](https://cython.readthedocs.io/en/stable/index.html) python implementation
  - Optimized [Cythonized](https://cython.readthedocs.io/en/stable/index.html) implementation
  > Files [ackermann.py](python/src/ackermann/ackermann.py), [ackermann_numba.py](python/src/ackermann/ackermann_numba.py) almost identical, but performance is significantly different.

# Benchmarks
- [c benchmark](c/benchmark.md)
- [go benchmark](go/benchmark.md)
- [js benchmark](js/benchmark.md)
- [python benchmark](python/benchmark.md)

To benchmark all available algorithms, run next command.
```shell
docker compose up
```
This command will build and run benchmarks in docker container and will update files in [results](results) directory.