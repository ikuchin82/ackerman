![](https://kroki.io/vegalite/svg/eNq1lk1vozAQhv9K5dOulB1hbAzk1vtKPeyxqioHnJQW7Cxf22zEf1-73aoukIaAe0hCbOd5Pe_MOD6ilNccrY-o5XkjKrS-PSKlPxDP95kUaIVyLnf6e_LyuBG5mUyuflwpX49spVmaPN2XImnKKmvFN7q6wt_1VIvWHkSsW80D5qLVU_dVU9jEGAhdSCz488c9xsEFROI6aDIp6GAhsRc0Bg-fJO6UjdwpzTwbdASBPxdY1frdhvkLYOMOBsDChcSegz74px18rGziY6WJUqXi06ADCKMlwPFthvBeOanYZFwu62hdOGwucDw12O7oecRBcVN8AZFMCXo28FTQF9k4qaMxOUmc09EMIjoXONLRBM-FjTtIgQULiYOOJqeJczqaAV3CG99lAKeN3B_qByVt8OuIhhPAeEIRUQwscsjvJc3HFLzABX9gNsYBBPFM9IRDxfcCIKFDfs8a4kXAmAv-0Bo_tP-sLkNPOXqiCCh1yO9XDSNAfRf8oTWUwFzTCSRv8yMG6Veeq-TjER1G7rW0WSNaxL5CuYyrdwf6kng-asShQxFdAXWW3Le8rHqpWVoG2V-RnreMMmDka5T6Vwzd9cx3LDV-PcLgU8dCg06NIZ6focesnnCM4WhJHOMa_aTEFGjoSmN4nHnhZ9eic3jZFBs-5aoZx841ekYx8BxqjJetB17oTmOQDDqz0_eH_eF8EmIgLul9-3WOsQv8sEQJxLS761aoLrmstqos0Pr2boUKXj7pH214qRcKmag0M2JHdDBv20zkqZ5u9WR92Av9-Lvhss7Med6ae7N-zM1w2ejRQiCt8Gz_ciutRdtGJnWmY9CrEpWrcrjyVUSqIpM8N7GKnZCpWafKTMja7FXVtSresDdvw7nY1gbcF6vMYKn-2FqqsnZ180t_eRA8FS_7eXH3Wu7MnB963etem0LagP8peIf8NDnquu4fhsLpZQ==)
# OS alpine
## c o2
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|0.86|
|ack_levels_sum(4, 1)|9.34|
|ack_levels_max(4, 1)|0.95|
## c o3
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|0.86|
|ack_levels_sum(4, 1)|9.35|
|ack_levels_max(4, 1)|1.01|
## go 
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|8.52|
|ack_stack(4, 1)|2.52|
|ack_levels_sum(4, 1)|5.67|
|ack_levels_max(4, 1)|2.21|
## js node
|Function|Runtime|
|-|-|
|ack_stack(4, 1)|5.78|
|ack_levels_max(4, 1)|7.5|
# OS debian
## c o2
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|1.06|
|ack_levels_sum(4, 1)|1.34|
|ack_levels_max(4, 1)|1.41|
## c o3
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|1.01|
|ack_levels_sum(4, 1)|1.06|
|ack_levels_max(4, 1)|1.13|
## go 
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|6.84|
|ack_stack(4, 1)|2.31|
|ack_levels_sum(4, 1)|4.65|
|ack_levels_max(4, 1)|2.35|
## js node
|Function|Runtime|
|-|-|
|ack_stack(4, 1)|6.4|
|ack_levels_max(4, 1)|5.1|
## python 3.11
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|141.68|
|ack_levels_sum(4, 1)|214.05|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|115.59|
## python 3.12
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|205.37|
|ack_levels_sum(4, 1)|308.66|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|127.21|
## python 3.13
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|188.44|
|ack_levels_sum(4, 1)|263.42|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|143.6|
## python 3.13.cython
|Function|Runtime|
|-|-|
|ack_levels_max_malloc(4, 1)|1.78|
|ack_levels_sum_malloc(4, 1)|3.52|
|ack_levels_max(4, 1)|2.2|
|ack_levels_sum(4, 1)|2.97|
|ack_static_vars(4, 1)|1.76|
## python 3.13.cythonized
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|46.63|
|ack_levels_sum(4, 1)|127.62|
|ack_recursive(4, 1)|11.24|
|ack_stack(4, 1)|9.96|
## python 3.13.jit
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|118.24|
|ack_levels_sum(4, 1)|194.47|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|107.65|
## python 3.13.numba
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|1.99|
|ack_levels_sum(4, 1)|6.09|
|ack_recursive(4, 1)|10.07|
|ack_stack(4, 1)|4.63|
## python pypy
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|9.33|
|ack_levels_sum(4, 1)|61.91|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|13.94|