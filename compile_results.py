import base64
import json
import re
import zlib
from pathlib import Path
from typing import Optional

pwd = Path(__file__).parent


def generate_vega_lite_chart(
    os_list: Optional[list[str]] = None,
    lang_list: Optional[list[str]] = None,
    fn_list: Optional[list[str]] = None,
):
    re_pattern = re.compile(r"(\d+\.\d+).*")
    os = lang = labels = None

    chart_data = {
        "data": {"values": []},
        "transform": [],
        "mark": "bar",
        "encoding": {
            "y": {"field": "v", "type": "quantitative", "title": "runtime"},
            "x": {"field": "fn", "title": "function"},
            "color": {
                "field": "fn",
                "type": "nominal",
                "legend": {"orient": "bottom", "titleOrient": "left"},
                "title": "functions",
            },
            "row": {"field": "os", "title": "OS", "header": {"labelAngle": 270}},
            "column": {"field": "labels", "title": "Lang"},
        },
    }

    if os_list:
        chart_data["transform"].append({"filter": {"field": "os", "oneOf": os_list}})

    if lang_list:
        chart_data["transform"].append(
            {"filter": {"field": "lang", "oneOf": lang_list}}
        )

    if fn_list:
        chart_data["transform"].append({"filter": {"field": "fn", "oneOf": fn_list}})

    unique_fn_names = set()

    # files = sorted(pwd.glob("results/*.txt"))[:3]
    files = sorted(pwd.glob("results/*.txt"), key=lambda x: x.name[:-4])

    md_lines = []

    for file in files:
        os_new, lang_new, *labels_new = file.name[:-4].split(".", maxsplit=2)
        labels_new = " ".join(labels_new)

        if lang_list and lang_new not in lang_list:
            continue

        if os_new != os:
            md_lines.extend([f"# OS {os_new}"])
        if os_new != os or lang != lang_new or labels != labels_new:
            md_lines.extend(
                [f"## {lang_new} {labels_new}", "|Function|Runtime|", "|-|-|"]
            )

        os = os_new
        lang = lang_new
        labels = labels_new

        lines = file.read_text().splitlines()
        for line in lines:
            fn_name, execution_time = line.split(": ", maxsplit=1)
            unique_fn_names.add(fn_name)
            if number_list := re_pattern.findall(execution_time):
                # print(labels, fn_name, float(number_list[0]))
                runtime = float(number_list[0])
                chart_data["data"]["values"].append(
                    {
                        "os": os,
                        "lang": lang,
                        "labels": f"{lang} - {labels_new}",
                        "fn": fn_name,
                        "v": round(runtime, 2),
                    }
                )
                md_lines.extend([f"|{fn_name}|{round(runtime, 2)}|"])
            else:
                md_lines.extend([f"|{fn_name}|{execution_time}|"])

    json_data = json.dumps(chart_data, separators=(",", ":"))
    json_data_encoded = base64.urlsafe_b64encode(
        zlib.compress(json_data.encode("utf-8"), 9)
    ).decode("ascii")

    # print(json.dumps(chart_data, indent=2, sort_keys=True))
    # print(f"https://kroki.io/vegalite/svg/{json_data_encoded}")
    md_lines.insert(0, f"![](https://kroki.io/vegalite/svg/{json_data_encoded})")

    return "\n".join(md_lines)


if __name__ == "__main__":
    (pwd / "benchmark.md").write_text(generate_vega_lite_chart())
    (pwd / "c" / "benchmark.md").write_text(generate_vega_lite_chart(lang_list=["c"]))
    (pwd / "go" / "benchmark.md").write_text(generate_vega_lite_chart(lang_list=["go"]))
    (pwd / "js" / "benchmark.md").write_text(generate_vega_lite_chart(lang_list=["js"]))
    (pwd / "python" / "benchmark.md").write_text(
        generate_vega_lite_chart(
            lang_list=["python"],
            fn_list=[
                "ack_levels_max(4, 1)",
                # "ack_levels_max_malloc(4, 1)",
                "ack_levels_sum(4, 1)",
                # "ack_levels_sum_malloc(4, 1)",
                "ack_recursive(4, 1)",
                "ack_stack(4, 1)",
                "ack_static_vars(4, 1)",
            ],
        )
    )
