#include <assert.h>
#include <stdio.h>
#include <time.h>

int ack_recursive(int m , int n){
	int result = 0;
    if(m == 0){
    	result = n + 1;
    }else if(n == 0){
    	result = ack_recursive(m-1, 1);
    } else {
    	result = ack_recursive(m-1, ack_recursive(m, n-1));
    }

    return result;
}

//int ack_stack(int m , int n) {
//	int last_m[0];
//	while (1) {
//		if(m == 0){
//			n += 1;
//        	if(last_m.length === 0){
//		        return n;
//        	} else {
//        		m = last_m.pop();
//	       	}
//		} else if(n == 0){
//            m -= 1;
//            n = 1;
//		} else {
//	        last_m.push(m - 1);
//            n -= 1;
//		}
//    }
//}

int ack_levels_max(int m ,int n) {
	int levels[] = {0, 0, 0, 0};
	int max_m = 4;
	while (1) {
		if(m == 0){
			n += 1;
        	while (levels[m] == 0) {
        	    m += 1;
        	    if(m == max_m){
        	        return n;
        	    }
            }
            levels[m] -= 1;
		} else if(n == 0){
            m -= 1;
            n = 1;
		} else {
	        levels[m - 1] += 1;
            n -= 1;
		}
    }
}

int ack_levels_sum(int m ,int n) {
	int levels[] = {0, 0, 0, 0};
	int max_m = 4;
    int v;
    int i;

	while (1) {
		if(m == 0){
            n += 1;
            v = 0;
            for(i=0;i<4;++i){
                v += levels[i];
            }
            if(v == 0){
                return n;
            }
            while(1){
                if(levels[m] > 0){
                    levels[m] -= 1;
                    break;
                }
                m += 1;
            }
		} else if(n == 0){
            m -= 1;
            n = 1;
		} else {
	        levels[m - 1] += 1;
            n -= 1;
		}
    }
}

int main()
{
	clock_t t1 = clock();
	assert(ack_recursive(4, 1) == 65533);
	clock_t t2 = clock();
	printf("ack_recursive(4, 1): %.4f\n", 1.0*(t2-t1)/CLOCKS_PER_SEC);

	t1 = clock();
	assert(ack_levels_sum(4, 1) == 65533);
	t2 = clock();
	printf("ack_levels_sum(4, 1): %.4f\n", 1.0*(t2-t1)/CLOCKS_PER_SEC);

	t1 = clock();
	assert(ack_levels_max(4, 1) == 65533);
	t2 = clock();
	printf("ack_levels_max(4, 1): %.4f\n", 1.0*(t2-t1)/CLOCKS_PER_SEC);
	return 0;
}