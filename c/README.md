## Compiling file
Compiling with O2 optimizations. 
```shell
gcc -O2 -o ackerman_with_o2_optimizations ackerman.c
````
Compiling with O3 optimizations.
```shell
gcc -O3 -o ackerman_with_o3_optimizations ackerman.c
```

## Test results
