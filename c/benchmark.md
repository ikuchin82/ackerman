![](https://kroki.io/vegalite/svg/eNqtk01vwjAMhv8KymmTOkQpsMFt90kcdpwQCq3DIhKHpWkHqvLf5xQxysckKDs18es89murFcu442xSsZKrAnI2-aiYoQ_jai0RWMQUxyXd0_q4ABXEtPPUMX2KCAyp6WpuIS1sLkt4GESd-JGkkk163ZeRj9oBFZQkzfNCN4njbjK4k6j55rjH8fAGYvLfppOrTA_vJJ6Yjru9-JeYwUJyvG_TBBy1BV42HTc33Y54Znpwi-nkGtOtgX-ZvmmMV206TvzMR8xZjrkwVte_uJDKgQ3_vZCgMkLURSJmEKaCUqjazPtZxDS3K5IX3JIKmJpMhm4qtm2-Lkl02zXQ8avg6KTjjqYVotKpELYFRTUwamXTfCmwkSQKTJ00GLJSo4w9z9wVQaMlchUmAkvALOQZKwFd6NU4Z_QeO92HFQgXwKfF8hC05rtZixZwSJy-0-UTeLabWL2DV1wGrf_c87teC43H46wXdYC8hfl6738ANSKgMA==)
# OS alpine
## c o2
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|0.86|
|ack_levels_sum(4, 1)|9.34|
|ack_levels_max(4, 1)|0.95|
## c o3
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|0.86|
|ack_levels_sum(4, 1)|9.35|
|ack_levels_max(4, 1)|1.01|
# OS debian
## c o2
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|1.06|
|ack_levels_sum(4, 1)|1.34|
|ack_levels_max(4, 1)|1.41|
## c o3
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|1.01|
|ack_levels_sum(4, 1)|1.06|
|ack_levels_max(4, 1)|1.13|