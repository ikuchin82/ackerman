from numba import njit


@njit
def ack_recursive(m: int, n: int) -> int:
    if m == 0:
        return n + 1
    elif n == 0:
        return ack_recursive(m - 1, 1)
    else:
        return ack_recursive(m - 1, ack_recursive(m, n - 1))


@njit
def ack_stack(m: int, n: int) -> int:
    last_m = []
    while True:
        if m == 0:
            n += 1
            if len(last_m) == 0:
                return n
            else:
                m = last_m.pop()
        elif n == 0:
            m -= 1
            n = 1
        else:
            last_m.append(m - 1)
            n -= 1


@njit
def ack_levels_sum(m: int, n: int) -> int:
    levels = [0] * max(m, 1)
    while True:
        if m == 0:
            n += 1
            if sum(levels) == 0:
                return n
            while True:
                if levels[m] > 0:
                    levels[m] -= 1
                    break
                m += 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1


@njit
def ack_levels_max(m: int, n: int) -> int:
    max_m = m or 1
    levels = [0] * max_m
    while True:
        if m == 0:
            n += 1
            while levels[m] == 0:
                m += 1
                if m == max_m:
                    return n
            levels[m] -= 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1
