"""
This code used without any changes to produce "Cythonized" version this is 2x-10x faster,
 because of that it uses types from "cython" and not from "typing".
"""

import cython


@cython.ccall
def ack_recursive(m: cython.int, n: cython.int) -> cython.int:
    if m == 0:
        return n + 1
    elif n == 0:
        return ack_recursive(m - 1, 1)
    else:
        return ack_recursive(m - 1, ack_recursive(m, n - 1))


def ack_stack(m: cython.int, n: cython.int) -> cython.int:
    last_m = []
    while True:
        if m == 0:
            n += 1
            if len(last_m) == 0:
                return n
            else:
                m = last_m.pop()
        elif n == 0:
            m -= 1
            n = 1
        else:
            last_m.append(m - 1)
            n -= 1


def ack_levels_sum(m: cython.int, n: cython.int) -> cython.int:
    levels = [0] * max(m, 1)
    while True:
        if m == 0:
            n += 1
            if sum(levels) == 0:
                return n
            while True:
                if levels[m] > 0:
                    levels[m] -= 1
                    break
                m += 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1


def ack_levels_max(m: cython.int, n: cython.int) -> cython.int:
    levels = [0] * max(m, 1)
    max_m = len(levels)
    while True:
        if m == 0:
            n += 1
            while levels[m] == 0:
                m += 1
                if m == max_m:
                    return n
            levels[m] -= 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1
