from argparse import ArgumentParser
from datetime import timedelta
from importlib import import_module
from time import perf_counter


def main(module_name: str, m: int = 4, n: int = 1) -> None:
    module = import_module(f"ackermann.{module_name}")

    for fn_name, fn in sorted(
        (k, v) for k, v in module.__dict__.items() if k.startswith("ack_")
    ):
        print(f"{fn_name}({m}, {n}): ", end="", flush=True)
        try:
            t1 = perf_counter()
            assert fn(m, n) == 65533
            t2 = perf_counter()
            print(f"{timedelta(seconds=t2 - t1).total_seconds():.4f}")
        except Exception as err:
            print(f"{err.__class__.__name__} {err}")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("module_name")
    args = parser.parse_args()

    main(module_name=args.module_name)
