from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free


# def ack_stack_vector(int m, int n):
#     cdef vector[int] last_m
#     while True:
#         if m == 0:
#             n += 1
#             if len(last_m) == 0:
#                 return n
#             else:
#                 m = last_m.back()
#                 last_m.pop_back()
#         elif n == 0:
#             m -= 1
#             n = 1
#         else:
#             last_m.push_back(m - 1)
#             n -= 1

def ack_levels_sum(int m, int n) :
    cdef int levels[10]
    cdef int max_m = m

    for x in range(len(levels)):
        levels[x] = 0

    cdef int i = 0
    cdef int v = 0

    while True:
        if m == 0:
            n += 1
            v = 0
            for i in range(max_m):
                v += levels[i]
            if v == 0:
                return n
            while True:
                if levels[m] > 0:
                    levels[m] -= 1
                    break
                m += 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1

def ack_levels_max(int m, int n):
    cdef int levels[10]
    cdef int max_m = m

    for x in range(len(levels)):
        levels[x] = 0

    while True:
        if m == 0:
            n += 1
            while levels[m] == 0:
                m += 1
                if m == 4:
                    return n
            levels[m] -= 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1


def ack_levels_sum_malloc(int m, int n):
    cdef int *levels = <int *> PyMem_Malloc(m * sizeof(int))
    cdef int max_m = m

    for x in range(m):
        levels[x] = 0

    cdef  int v = 0
    cdef  int i = 0

    while True:
        if m == 0:
            n += 1
            v = 0
            for i in range(max_m):
                v += levels[i]
            if v == 0:
                PyMem_Free(levels)
                return n
            while True:
                if levels[m] > 0:
                    levels[m] -= 1
                    break
                m += 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1

def ack_levels_max_malloc(int m, int n):
    if m == 0:
        return n + 1

    cdef int *levels = <int *> PyMem_Malloc(m * sizeof(int))
    cdef int max_m = m

    for x in range(m):
        levels[x] = 0

    while True:
        if m == 0:
            n += 1
            while levels[m] == 0:
                m += 1
                if m == max_m:
                    PyMem_Free(levels)
                    return n
            levels[m] -= 1
        elif n == 0:
            m -= 1
            n = 1
        else:
            levels[m - 1] += 1
            n -= 1


def ack_static_vars(int m, int n):
    cdef int level_0 = 0
    cdef int level_1 = 0
    cdef int level_2 = 0
    cdef int level_3 = 0
    while True:
        if m == 0:
            n += 1
            if level_0 + level_1 + level_2 + level_3 == 0:
                break
            else:
                if level_0 > 0:
                    m = 0
                    level_0 -= 1
                elif level_1 > 0:
                    m = 1
                    level_1 -= 1
                elif level_2 > 0:
                    m = 2
                    level_2 -= 1
                elif level_3 > 0:
                    m = 3
                    level_3 -= 1
        elif n == 0:
            m -= 1
            n = 1
        else:

            if m - 1 == 0:
                level_0 += 1
            elif m - 1 == 1:
                level_1 += 1
            elif m - 1 == 2:
                level_2 += 1
            elif m - 1 == 3:
                level_3 += 1

            n -= 1

    return n
