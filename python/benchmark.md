![](https://kroki.io/vegalite/svg/eNq1VsuO2jAU_RXkVSulVh7Oi133lVjMskLIJA7jjmMziZMORfn3XkPpkMcIlDELHr6-nHPvOdcmR5RTTdHyiFoqGlaj5c8jUvCBcrblVCIHCSp3sN4f9LM6r7dM1P8ji2-LAHsebBQSgjR72QjWQsampG9fiLPwvsJei5Ye8XCUdI41_Lopr_F9j2A3tIFfa3jvle6FOExnQvu3pfHdEAexRfyBNIGb4CiygT-Wxo-x782EDu6YmiTBhFjEH05NFGDi28AfS0MCPFf0AGeX_QmB4CWEynpkOE7sc4FYE1wBDv2H9NUzBvsP6afPkcYWSWACNM82La3qgTWfHQP-h-W3JSMRjoLHMA2EM6c-8i1TVSxrqpq3rH_1Yp9YJhqd1BSn8x36xfUd15iXfKaPaY6hKSnBJLbFMb7OXPA8nA0vm3JL7xAKp6l1joFQEXYtckyPrYvd2B7HyAwy86TvD_vDbRNSHNhEH8oPHns24McjGuCUdOvOQbqisi5UVZ4eaAsuNKvMU27Bmcjh5ycyBynJVgWkXFjXnalrnF7I6-QPhPug4-kZGdc__Q8CFa0dVNLqBcrY0goSmcxUzo1YR3S4rrKFTX3YM_j62lCpuUFrmYlyLUy4aiBaMgQKvY36uyQVjcw0BzUgK1NCjZX4RyJVySUVxiu2YzI3eariTGpTq9JalRfY1SUsWKEN8JCsNsFK_b7mgvl4T1w9weKZ0fzszGk6vsud2fNjtzvX2pSy7_JphN5Bfhjbu677C3ZL9d0=)
# OS debian
## python 3.11
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|141.68|
|ack_levels_sum(4, 1)|214.05|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|115.59|
## python 3.12
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|205.37|
|ack_levels_sum(4, 1)|308.66|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|127.21|
## python 3.13
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|188.44|
|ack_levels_sum(4, 1)|263.42|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|143.6|
## python 3.13.cython
|Function|Runtime|
|-|-|
|ack_levels_max_malloc(4, 1)|1.78|
|ack_levels_sum_malloc(4, 1)|3.52|
|ack_levels_max(4, 1)|2.2|
|ack_levels_sum(4, 1)|2.97|
|ack_static_vars(4, 1)|1.76|
## python 3.13.cythonized
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|46.63|
|ack_levels_sum(4, 1)|127.62|
|ack_recursive(4, 1)|11.24|
|ack_stack(4, 1)|9.96|
## python 3.13.jit
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|118.24|
|ack_levels_sum(4, 1)|194.47|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|107.65|
## python 3.13.numba
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|1.99|
|ack_levels_sum(4, 1)|6.09|
|ack_recursive(4, 1)|10.07|
|ack_stack(4, 1)|4.63|
## python pypy
|Function|Runtime|
|-|-|
|ack_levels_max(4, 1)|9.33|
|ack_levels_sum(4, 1)|61.91|
|ack_recursive(4, 1)|RecursionError maximum recursion depth exceeded|
|ack_stack(4, 1)|13.94|