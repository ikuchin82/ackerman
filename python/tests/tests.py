from unittest import TestCase

test_cases = {
    (0, 0): 1,
    (0, 1): 2,
    (0, 2): 3,
    (0, 3): 4,
    (0, 4): 5,
    (1, 0): 2,
    (1, 1): 3,
    (1, 2): 4,
    (1, 3): 5,
    (1, 4): 6,
    (2, 0): 3,
    (2, 1): 5,
    (2, 2): 7,
    (2, 3): 9,
    (2, 4): 11,
    (3, 0): 5,
    (3, 1): 13,
    (3, 2): 29,
    (3, 3): 61,
    (3, 4): 125,
}


class Test(TestCase):
    def test_pure_python(self):
        from ackermann import ackermann

        for fn_name, fn in [
            (k, v) for k, v in ackermann.__dict__.items() if k.startswith("ack_")
        ]:
            for given, expected in test_cases.items():
                with self.subTest(fn=fn_name, given=given, expected=expected):
                    assert fn(*given) == expected

    def test_numba(self):
        from ackermann import ackermann_numba

        for fn_name, fn in [
            (k, v) for k, v in ackermann_numba.__dict__.items() if k.startswith("ack_")
        ]:
            for given, expected in test_cases.items():
                with self.subTest(fn=fn_name, given=given, expected=expected):
                    assert fn(*given) == expected

    def test_cythonized(self):
        from ackermann import ackermann_cythonized

        for fn_name, fn in [
            (k, v)
            for k, v in ackermann_cythonized.__dict__.items()
            if k.startswith("ack_")
        ]:
            for given, expected in test_cases.items():
                with self.subTest(fn=fn_name, given=given, expected=expected):
                    assert fn(*given) == expected

    def test_cythonized_c(self):
        from ackermann import ackermann_cython

        for fn_name, fn in [
            (k, v) for k, v in ackermann_cython.__dict__.items() if k.startswith("ack_")
        ]:
            for given, expected in test_cases.items():
                with self.subTest(fn=fn_name, given=given, expected=expected):
                    assert fn(*given) == expected
                    pass
