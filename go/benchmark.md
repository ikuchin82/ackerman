![](https://kroki.io/vegalite/svg/eNqlUkFuwjAQ_AryqZXSqAQCiFvvlTj0WCFkknVqYa-pY6egyH_vOogSCidySbyz49nRrFtWcsfZsmUNVx5qtvxsmaEf42ovEVjCFMeK6sp05y2ouqtGLyMCBEZqsdtYKLytZQNP02Q0fqZWw5aLNM9C8phg7ejbF8sGiCloqLOpve4r5ulsPlBR88O1x2z8p1jCVnIcluAsXUwfFbyT4ORhd_cTnKazfKDiTYKTPKxDwpzlWAtjdfckhVQObHynQoIqSaIbkjCDsBJEidPWIawTprndUX_LLbUBC1PKaKdlx_71hpruuAc6fnuOTjruKPuISqcibD2hGhh5OfRvCuyRhMfCSYORVRhl7C3zNASNlshVTAQqwDLyjJWALno1zhl9ll2dYQXCReH_w-oIWvPTn0UbuBBXH1R8AS9PkXU7eMMq9rL5azh59Rqv8-wWdRF5jwGHEH4BVmlBpw==)
# OS alpine
## go 
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|8.52|
|ack_stack(4, 1)|2.52|
|ack_levels_sum(4, 1)|5.67|
|ack_levels_max(4, 1)|2.21|
# OS debian
## go 
|Function|Runtime|
|-|-|
|ack_recursive(4, 1)|6.84|
|ack_stack(4, 1)|2.31|
|ack_levels_sum(4, 1)|4.65|
|ack_levels_max(4, 1)|2.35|