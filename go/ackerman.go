package main

import "fmt"
import "time"

func ack_recursive(m int, n int) int {
	result := 0
    if m == 0 {
    	result = n + 1
    } else if n == 0 {
    	result = ack_recursive(m-1, 1)
    } else {
    	result = ack_recursive(m-1, ack_recursive(m, n-1))
    }

    return result
}

func ack_stack(m int, n int) int {
	answer := 0
	last_m_len := 0
	last_m := make([]int, 0)
	for {
		if m == 0 {
			answer = n + 1
			last_m_len = len(last_m)
        	if last_m_len == 0 {
		        break
        	} else {
        		m = last_m[last_m_len-1]
        		last_m = last_m[:last_m_len-1]
        		n = answer
	       	}
		} else if n == 0 {
            m -= 1
            n = 1
		} else {
	        last_m = append(last_m, m - 1)
            n -= 1
		}
    }
    return answer
}

func ack_levels_sum(m int, n int) int {
	var levels [4]int
	max_m := m
	i := 0
	v := 0
	for {
		if m == 0 {
			n += 1
			v = 0
        	for i=0; i != max_m; i++ {
                v += levels[i]
            }
        	if v == 0 {
		        return n
        	}
        	for {
        		if levels[m] > 0 {
        			levels[m] -= 1
        			break
        		}
        		m += 1
	       	}
		} else if n == 0 {
            m -= 1
            n = 1
		} else {
	        levels[m - 1] += 1
            n -= 1
		}
    }
}

func ack_levels_max(m int, n int) int {
	levels := make([]int, m)
	max_m := m
	for {
		if m == 0 {
			n += 1
            for levels[m] == 0 {
                m += 1
                if m == max_m {
                    return n
                }
            }
            levels[m] -= 1
		} else if n == 0 {
            m -= 1
            n = 1
		} else {
	        levels[m - 1] += 1
            n -= 1
		}
    }
}

func main() {
	t0 := time.Now()
	if ack_recursive(4,1) != 65533 {panic("Test didn't pass")}
	t1 := time.Now()
	fmt.Println("ack_recursive(4, 1):", t1.Sub(t0))

	t0 = time.Now()
	if ack_stack(4,1) != 65533 {panic("Test didn't pass")}
	t1 = time.Now()
	fmt.Println("ack_stack(4, 1):", t1.Sub(t0))

	t0 = time.Now()
	if ack_levels_sum(4,1) != 65533 {panic("Test didn't pass")}
	t1 = time.Now()
	fmt.Println("ack_levels_sum(4, 1):", t1.Sub(t0))

	t0 = time.Now()
	if ack_levels_max(4,1) != 65533 {panic("Test didn't pass")}
	t1 = time.Now()
	fmt.Println("ack_levels_max(4, 1):", t1.Sub(t0))
}
